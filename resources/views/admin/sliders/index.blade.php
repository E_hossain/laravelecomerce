<x-admin.master>
    <div class="col-12 col-lg-11 col-xxl-9 d-flex">
        <div class="card flex-fill">
            <div class="card-header">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2> create Slider</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('sliders.create') }}"> Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if (session('message'))
                    <div class=" alert alert-success">
                        <span class="close" data-dismiss="alert"> </span>
                        <strong>{{ session('message') }}</strong>
                    </div>
                @endif

            <table class="table table-hover my-0">
                <thead>
                <tr>
                    <th>SL</th>
                    <th class="d-none d-xl-table-cell">Title</th>
                    <th class="d-none d-xl-table-cell">SortTitle</th>
                    <th class="d-none d-xl-table-cell">image</th>
                    <th class="d-none d-md-table-cell">Action</th>
                </tr>
                </thead>
                <tbody>
    @foreach($sliders as $key => $slider)
        <tr>
            <td> {{ $key+1 }} </td>
            <td>{{ $slider->slider_title }}</td>
            <td>{{ $slider->short_title }}</td>
            <td>  <img src="/storage/sliders/{{($slider->slider_image) }}" style="width: 70px; height:40px;" >  </td>

            <td>
                <a class="btn btn-info btn-sm"
                   href="{{ route('sliders.show', ['slider' => $slider->id]) }}">Show</a>
                <a class="btn btn-info btn-sm"
                   href="{{ route('sliders.edit', ['slider' => $slider->id]) }}">Edit</a>
                <form style="display:inline"
                      action="{{ route('sliders.destroy',['slider' => $slider->id]) }}"
                      method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm"
                            onclick="return confirm('are sure want delete?')">Delete</button>
                </form>

            </td>
        </tr>

    @endforeach
                </tbody>
            </table>

            </div>
        </div>
    </div>
</x-admin.master>
