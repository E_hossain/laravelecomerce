<x-admin.master>

    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2> Show Slider</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('sliders.index') }}"> Back</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    {{ $slider->slider_title }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Details:</strong>
                    {{ $slider->short_title }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Image:</strong>
                    <img src="/storage/sliders/{{($slider->slider_image) }}" style="width: 70px; height:40px;" >
                </div>
            </div>
        </div>

    </div>

</x-admin.master>


