<x-admin.master>

    <div class="container-fluid p-0">

    <form action="{{ route('sliders.update',['slider'=>$slider->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('patch')
    <div class="mb-3 row">
<label for="inputTitle" class="col-sm-3 col-form-label">Slider</label>
    <div class="col-sm-9">
        <input
            type="text"
            class="form-control"
            id="inputTitle"
            name="slider_title"
            value="{{old('slider_title',$slider->slider_title)}}">
         @error('slider_title')
         <p class="text-danger">{{ $message }}</p>
        @enderror
    </div>
    </div>


            <div class="mb-3 row">
                <label for="inputdetails" class="col-sm-3 col-form-label">Details</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputdetails"
                        name="short_title"
                        value="{{old('short_title',$slider->short_title)}}">
                                        @error('short_title')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputImg" class="col-sm-3 col-form-label">Image</label>
                <div class="col-sm-9">
                    <input
                        type="file"
                        class="form-control"
                        id="inputImg"
                        name="slider_image"
                        value="{{old('slider_image',$slider->slider_image)}}">
                                        @error('slider_image')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                </div>

            </div>


            <div class="mb-3 row">
                <div class="col-sm-9 offset-3">
                    <button type="submit" class="btn btn-info">update</button>
                </div>

            </div>

        </form>
    </div>

</x-admin.master>


