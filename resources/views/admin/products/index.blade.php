<x-admin.layouts.admin_master>
    <div class="col-12 col-lg-11 col-xxl-9 d-flex">
        <div class="card flex-fill">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">

                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('products.create') }}"> Back
                                <i class="fa fa-home"></i> <span class="badge rounded-pill bg-danger"> {{ count($products) }} </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if (session('message'))
                    <div class=" alert alert-success">
                        <span class="close" data-dismiss="alert"> </span>
                        <strong>{{ session('message') }}</strong>
                    </div>
                @endif

            <table class="table table-hover my-0">
                <thead>
                <tr>
                    <th>SL</th>
                    <th class="d-none d-xl-table-cell">ProductName</th>
                    <th class="d-none d-xl-table-cell">Picture</th>
                    <th class="d-none d-xl-table-cell">Price</th>
                    <th class="d-none d-xl-table-cell">Discount</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
    @foreach($products as $key => $product)
        <tr>
            <td> {{ $key+1 }} </td>
            <td>{{ $product->product_name }}</td>
            <td> <img src="/storage/product/{{$product->product_image}}" style="width: 70px; height:40px;" >  </td>
            <td>{{ $product->selling_price }}</td>
            <td>
                @if($product->discount_price == NULL)
                    <span class="badge rounded-pill bg-info">No Discount</span>
                @else
                    @php
                        $amount = $product->selling_price - $product->discount_price;
                        $discount = ($amount/$product->selling_price) * 100;
                    @endphp
                    <span class="badge rounded-pill bg-danger"> {{ round($discount) }}%</span>
                @endif
            </td>

           <td> @if($product->status == 1)
                    <span class="badge rounded-pill bg-success">Active</span>
                @else
                    <span class="badge rounded-pill bg-danger">InActive</span>
                @endif
            </td>

<td>
    <a href="{{ route('products.show', ['product' => $product->id]) }}"><i class="fa fa-eye"></i></a>
    <a href="{{ route('products.edit', ['product' => $product->id]) }}"><i class="fa fa-pencil"></i></a>
    <form style="display:inline"
          action="{{ route('products.destroy',['product' => $product->id]) }}"
          method="post">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger btn-sm"
onclick="return confirm('are sure want delete?')"><i class="fa fa-trash"></i></button>

@if($product->status == 1)
    <a href="{{route('product.active',$product->id)}}" class="btn btn-primary" title="Active"><i class="fa-solid fa fa-thumbs-up"></i></a>
@else
    <a href="{{route('product.inactive',$product->id)}}" class="btn btn-primary" title="Inactive"><i class="fa-solid fa fa-thumbs-down"></i></a>
@endif
    </form>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
</x-admin.layouts.admin_master>
