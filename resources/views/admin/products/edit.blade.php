<x-admin.layouts.admin_master>

    <div class="container-fluid p-0">

        <form action="{{ route('products.update',['product'=>$product->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <input type="hidden" name="id" value="{{$product->id}}">
            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">ProductName</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="product_name"
                        value="{{$product->product_name}}">
                    @error('product_name')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">ProductCode</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="product_code"
                        value="{{$product->product_code}}">
                    @error('product_code')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">ProductQty</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="product_qty"
                        value="{{$product->product_qty}}">
                    @error('product_qty')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">ProductTags</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="product_tags"
                        value="{{$product->product_tags}}">
                    @error('product_tags')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">ProductSize</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="product_size"
                        value="{{$product->product_size}}">
                    @error('product_size')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">ProductColor</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="product_color"
                        value="{{$product->product_color}}">
                    @error('product_color')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">Prize</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="selling_price"
                        value="{{$product->selling_price}}">
                    @error('selling_price')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">ProductDiscount</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="discount_price"
                        value="{{$product->discount_price}}">
                    @error('discount_price')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">ShortDescription</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="short_descp"
                        value="{{$product->short_descp}}">
                    @error('short_descp')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="mb-3 row">
                <label for="mytextarea" class="col-sm-3 col-form-label">longDescription</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="long_descp" placeholder="Leave a comment here" id="mytextarea">
                        {!! $product->long_descp !!}</textarea>
                    </textarea>

                </div>
            </div>



            <div class="mb-3 row">
                <label for="inputImg" class="col-sm-3 col-form-label">Image</label>
                <div class="col-sm-9">
                    <input
                        type="file"
                        class="form-control"
                        id="inputImg"
                        name="product_thambnail"
                        value=""
                    >
                    @error('product_thambnail')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

            </div>
            <div class="mb-3 row">
                <label for="inputImg" class="col-sm-3 col-form-label">product</label>
                <div class="col-sm-9">
            <select name="brand_id" class="form-select">
                <option></option>
                @foreach($brands as $brand)
                    <option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
                    <option value="{{ $brand->id }}" {{ $brand->id == $product->brand_id ? 'selected' : '' }} >{{ $brand->brand_name }}</option>
                @endforeach
            </select>
            </div>
            </div>

            <div class="mb-3 row">
                <label for="inputImg" class="col-sm-3 col-form-label">Category</label>
                <div class="col-sm-9">
                    <select name="category_id" class="form-select">
                        <option></option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                            <option value="{{ $category->id }}" {{ $category->id == $product->category_id ? 'selected' : '' }}>{{ $category->category_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="inputImg" class="col-sm-3 col-form-label">Vendor</label>
                <div class="col-sm-9">
                    <select name="vendor_id" class="form-select">
                        <option selected>Open this select menu</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        <div class="row g-3">
            <div class="col-md-6">
                <div class="form-check">
                    <input class="form-check-input" name="hot_deals" type="checkbox" value="1" id="flexCheckDefault" {{ $product->hot_deals == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="flexCheckDefault"> Hot Deals</label>
                </div>
            </div>
        </div>
            <div class="row-cols-3">
                <div class="col-md-6">
                    <div class="form-check">
                        <input class="form-check-input" name="special_deals" type="checkbox" value="1" id="flexCheckDefault"{{ $product->featured == 1 ? 'checked' : '' }}>
                        <label class="form-check-label" for="flexCheckDefault">Special Deals</label>
                    </div>
                </div>

            </div>
            <div class="row g-3">
                <div class="col-md-6">
                    <div class="form-check">
                        <input class="form-check-input" name="featured" type="checkbox" value="1" id="flexCheckDefault" {{ $product->featured == 1 ? 'checked' : '' }}>
                        <label class="form-check-label" for="flexCheckDefault">Featured</label>
                    </div>
                </div>
            </div>
            <div class="row-cols-3">
                <div class="col-md-6">
                    <div class="form-check">
                        <input class="form-check-input" name="special_offer" type="checkbox" value="1" id="flexCheckDefault" {{ $product->special_offer == 1 ? 'checked' : '' }}>
                        <label class="form-check-label" for="flexCheckDefault">Special Offer</label>
                    </div>
                </div>

            <div class="mb-3 row">
                <div class="col-sm-9 offset-3">
                    <button type="submit" class="btn btn-info">update</button>
                </div>

            </div>

        </form>
    </div>

</x-admin.layouts.admin_master>


