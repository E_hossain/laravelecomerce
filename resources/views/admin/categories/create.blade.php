<x-admin.layouts.admin_master>

    <div class="container-fluid p-0">

        <form action="{{ route('categories.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">category</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="category_name"
                        value="">
                    @error('category_name')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputImg" class="col-sm-3 col-form-label">Image</label>
                <div class="col-sm-9">
                    <input
                        type="file"
                        class="form-control"
                        id="inputImg"
                        name="category_image"
                        value=""
                    >
                    @error('category_image')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

            </div>


            <div class="mb-3 row">
                <div class="col-sm-9 offset-3">
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>

            </div>

        </form>
    </div>

</x-admin.layouts.admin_master>


