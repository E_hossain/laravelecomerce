<x-admin.layouts.admin_master>
    <div class="col-12 col-lg-11 col-xxl-9 d-flex">
        <div class="card flex-fill">
            <div class="card-header">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2> create category</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('categories.create') }}"> Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if (session('message'))
                    <div class=" alert alert-success">
                        <span class="close" data-dismiss="alert"> </span>
                        <strong>{{ session('message') }}</strong>
                    </div>
                @endif

            <table class="table table-hover my-0">
                <thead>
                <tr>
                    <th>SL</th>
                    <th class="d-none d-xl-table-cell">Title</th>
                    <th class="d-none d-xl-table-cell">image</th>
                    <th class="d-none d-md-table-cell">Action</th>
                </tr>
                </thead>
                <tbody>
    @foreach($categories as $key => $category)
        <tr>
            <td> {{ $key+1 }} </td>
            <td>{{ $category->category_name }}</td>
            <td> <img src="{{ asset($category->category_image) }}" style="width: 70px; height:40px;" >  </td>

            <td>
                <a class="btn btn-info btn-sm"
                   href="{{ route('categories.show', ['category' => $category->id]) }}">Show</a>
                <a class="btn btn-info btn-sm"
                   href="{{ route('categories.edit', ['category' => $category->id]) }}">Edit</a>
                <form style="display:inline"
                      action="{{ route('categories.destroy',['category' => $category->id]) }}"
                      method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm"
                            onclick="return confirm('are sure want delete?')">Delete</button>
                </form>

            </td>
        </tr>

    @endforeach
                </tbody>
            </table>

            </div>
        </div>
    </div>
</x-admin.layouts.admin_master>
