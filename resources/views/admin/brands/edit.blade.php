<x-admin.layouts.admin_master>

    <div class="container-fluid p-0">

        <form action="{{ route('brands.update',['brand'=>$brand->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="mb-3 row">
                <label for="inputTitle" class="col-sm-3 col-form-label">brand</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputTitle"
                        name="brand_name"
                        value="{{old('brand_name',$brand->brand_name)}}">
                                        @error('brand_name')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                </div>
            </div>


            <div class="mb-3 row">
                <label for="inputdetails" class="col-sm-3 col-form-label">Details</label>
                <div class="col-sm-9">
                    <input
                        type="text"
                        class="form-control"
                        id="inputdetails"
                        name="brand_slug"
                        value="{{old('brand_slug',$brand->brand_slug)}}">
                                        @error('brand_slug')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label for="inputImg" class="col-sm-3 col-form-label">Image</label>
                <div class="col-sm-9">
                    <input
                        type="file"
                        class="form-control"
                        id="inputImg"
                        name="brand_image"
                        value="{{old('brand_image',$brand->brand_image)}}">
                                        @error('brand_image')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                </div>

            </div>


            <div class="mb-3 row">
                <div class="col-sm-9 offset-3">
                    <button type="submit" class="btn btn-info">update</button>
                </div>

            </div>

        </form>
    </div>

</x-admin.layouts.admin_master>


