<x-guest-layout>
      <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                  <x-input-label for="name" :value="__('Name')" />
                  <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')"
                        required autofocus autocomplete="name" />
                  <x-input-error :messages="$errors->get('name')" class="mt-2" />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                  <x-input-label for="email" :value="__('Email')" />
                  <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"
                        required autocomplete="username" />
                  <x-input-error :messages="$errors->get('email')" class="mt-2" />
            </div>

            <!-- Password -->
            <div class="mt-4">
                  <x-input-label for="password" :value="__('Password')" />

                  <x-text-input id="password" class="block mt-1 w-full" type="password" name="password" required
                        autocomplete="new-password" />

                  <x-input-error :messages="$errors->get('password')" class="mt-2" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                  <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

                  <x-text-input id="password_confirmation" class="block mt-1 w-full" type="password"
                        name="password_confirmation" required autocomplete="new-password" />

                  <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
            </div>

            <div class="flex items-center justify-end mt-4">
                  <a class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800"
                        href="{{ route('login') }}">
                        {{ __('Already registered?') }}
                  </a>

                  <x-primary-button class="ml-4">
                        {{ __('Register') }}
                  </x-primary-button>
            </div>
      </form>
</x-guest-layout>


<x-frontend.layouts.master_dashboard>
      <!-- Session Status -->
      {{-- <x-auth-session-status class="mb-4" :status="session('status')" /> --}}

      <div class="container">

            <div class="wrap-breadcrumb">
                  <ul>
                        <li class="item-link"><a href="#" class="link">home</a></li>
                        <li class="item-link"><span>Register</span></li>
                  </ul>
            </div>
            <div class="row">
                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 col-md-offset-3">
                        <div class=" main-content-area">
                              <div class="wrap-login-item ">
                                    <div class="register-form form-item ">
                                          <form method="POST" action="{{ route('register') }}">
                                                @csrf
                                                <fieldset class="wrap-title">
                                                      <h3 class="form-title">Create an account</h3>
                                                </fieldset>
                                                <fieldset class="wrap-input">
                                                      <label for="name">Name*</label>
                                                      <input type="text" id="name" name="name"
                                                            placeholder="Enter your name*">
                                                </fieldset>
                                                <fieldset class="wrap-input">
                                                      <label for="email">Email Address*</label>
                                                      <input type="email" id="email" name="email"
                                                            placeholder="Email address">
                                                </fieldset>

                                                <fieldset class="wrap-input item-width-in-half left-item ">
                                                      <label for="password">Password *</label>
                                                      <input type="password" id="password" name="password"
                                                            placeholder="Password">
                                                </fieldset>
                                                <fieldset class="wrap-input item-width-in-half ">
                                                      <label for="password">Confirm Password *</label>
                                                      <input type="password" id="password" name="password_confirmation"
                                                            placeholder="new-password">
                                                </fieldset>
                                                <input type="submit" class="btn btn-sign" value="Register"
                                                      name="register">
                                                {{-- <x-primary-button class="ml-4">
        {{ __('Register') }}
                                                </x-primary-button> --}}
                                          </form>
                                    </div>
                              </div>
                        </div>
                        <!--end main products area-->
                  </div>
            </div>
            <!--end row-->

      </div>
      <!--end container-->
</x-frontend.layouts.master_dashboard>>